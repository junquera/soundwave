// Thanks https://marcgg.com/blog/2016/11/01/javascript-audio/
document.addEventListener("DOMContentLoaded", function(event) {
 
  var context = new AudioContext();
  var o = context.createOscillator();
  var  g = context.createGain();
  o.connect(g);
  g.connect(context.destination);
  o.start(0);
  
  function onFrequencyChange(freq) {
    o.frequency.value = freq;
  }

  function onGainChange(gain) {
    g.gain.value = gain;
  }
   
  var freq_slider = document.getElementById("frequency");
  freq_slider.value = 440;
  var gain_slider = document.getElementById("gain");
  gain_slider.value = 50;

  freq_slider.oninput = function() {
    onFrequencyChange(this.value);
  } 

  gain_slider.oninput = function() {
    onGainChange(this.value/100);
  }

  // https://developer.chrome.com/blog/autoplay/#webaudio
  // One-liner to resume playback when user interacted with the page.
  document.getElementById('play').addEventListener('click', function() {
    context.resume().then(() => {
      console.log('Playback resumed successfully');
    });
  });

  // https://developer.mozilla.org/en-US/docs/Web/API/Touch_events
  document.getElementById('beep').addEventListener('touchstart', function() {
    context.resume().then(() => {
      console.log('Playback resumed successfully');
    });
  });

  document.getElementById('beep').addEventListener('touchend', function() {
    context.suspend().then(() => {
      console.log('Playback resumed successfully');
    });
  });
});
